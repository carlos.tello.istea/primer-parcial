# Primer Parcial - Instrucciones

Este repositorio contiene las instrucciones para completar el primer parcial. A continuación se detallan los pasos necesarios para realizar los cambios en el proyecto y construir las imágenes Docker.


## ---------------- TAREA 1 ----------------
## -----------------------------------------


## Parte 1: Clonar el Repositorio

1. Configurar parametros de usuario
   ```sh
   git config --global user.name "Carlos Facundo Tello"
   git config --global user.email "carlos.tello@istea.com.ar"

2. Clona el repositorio usando el siguiente comando (reemplaza la URL con la de tu repositorio si es diferente):
   ```sh
   git clone https://gitlab.com/carlos.tello.istea/primer-parcial.git

3. Navega al directorio del repositorio.
   ```sh
   cd primer-parcial

## Parte 2: Realizar cambios en la rama dev y realizar el merge desde dev a master (o main)

1. Cambiar a la rama `dev`:
   ```sh
   git checkout dev

2. Modificar app.js:
   ```javascript
   res.end("Este es el server de Carlos Tello del entorno de DESARROLLO del primer parcial");

3. Añadir y confirmar los cambios:
   ```sh
   git add app.js
   git commit -m "V1.0.4"

4. Subir los cambios al repositorio remoto en la rama `dev`:
   ```sh
   git push origin dev

5. Cambiar a la rama master (o main):
   ```sh
   git checkout master

6. Realizar merge desde dev:
   ```sh
   git merge dev -m "dev to master (o main) v1.0.4"


## Parte 3: Realizar cambios en app.js de la rama master


1. Modificar app.js
   ```javascript
   res.end("Este es el server de Carlos Tello del entorno de PRODUCCION del primer parcial");

2. Añadir y confirmar los cambios:
   ```sh
   git add app.js
   git commit -m "V1.0.2"

3. Subir los cambios al repositorio remoto en la rama master (o main):
   ```sh
   git push origin master


## Parte 4: Crear y levantar contenedores Docker

1. Subir los cambios al repositorio remoto en la rama master (o main):
   ```sh
   git checkout dev

2. Construir la imagen Docker para dev:
   ```sh
   docker build -t parcial:dev .

3. Cambiar a la rama master (o main):
   ```sh
   git checkout master

4. Construir la imagen Docker para prod:
   ```sh
   docker build -t parcial:prod .

5.
   ```sh
   docker run -d --name parcial_dev -p 3000:3000 parcial:dev

   docker run -d --name parcial_prod -p 3001:3000 parcial:prod


## Parte 5: Configurar la red Docker y probar conectividad

1. Crear la red parcial:
   ```sh
   docker network create parcial

2. Conectar los contenedores a la red:
   ```sh
   docker network connect parcial parcial_dev

   docker network connect parcial parcial_prod

3. Inspeccionar la red para ver los nombres e IPs asignadas:
   ```sh
   docker network inspect parcial

4. Entrar en modo interactivo a uno de los contenedores y realizar un ping:

   ```sh
   docker exec -it parcial_dev bash
   ip a
   
   ping parcial_dev
   ping parcial_prod


## ---------------- TAREA 2 ----------------
## -----------------------------------------

## Parte 1: Clonar el Repositorio

1. Crear los contenedores.
   ```sh
   docker run -d -p 8080:8080 --name adminer adminer

   docker run -d -p 3306:3306 --name mysql-db -e MYSQL_ROOT_PASSWORD=istea1234 mysql:latest

2. Abrir la url (192.168.1.167:8080) de adminer y utilizarl las credenciales para acceder al servidor "mysql-db"



